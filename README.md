# Introduction à Git et Gitlab

**Stage URFIST Lyon**
**Durée 6h**
*24 mars 2022*

**Contact du formateur** : **pierre-antoine.bouttier@univ-grenoble-alpes.fr**

## Supports

Les liens vers les supports seront ajoutés à la fin de la formation. 

## Prérequis

### Installation des outils

Pour cette formation vous aurez besoin d'un navigateur, de git, d'un éditeur de texte ou de code, d'un terminal. Si vous ne savez pas ce qu'est un terminal, veuillez vous reporter en bas de page à la section *Rudiments d'utilisation d'un Terminal.*
**Ne vous inquiétez pas nous prendrons le temps de découvrir tous ces éléments ensemble.** 

#### Sous windows 

Sous windows, un éditeur de code puissant et accessible est Visual Studio Code (abrégé VSCode). Vous aurez notamment accès à un terminal au sein de VScode. 
Vous pouvez télécharger l'installateur à cette adresse : 

https://code.visualstudio.com/sha/download?build=stable&os=win32-user

Pour git, vous pouvez l'installer en téléchargeant et en ouvrant le fichier suivant : 
https://github.com/git-for-windows/git/releases/latest

Lors de l'installation, vous pouvez laisser les options cochées par défaut. 

#### Sous linux

En ce qui concerne le terminal sous linux, vous pouvez utiliser celui par défuat de votre distribution. 

Pour git, la procédure change en fonction de votre distribution. Par exemple, pour une distribution basée sur Debian (Ubuntu, linux-mint), il faut, dans un terminal, rentrer la commande suivante : 
```
sudo apt-get install git
```

Pour l'éditeur, je vous laisse le choix. [VSCode est aussi disponible sous linux.](https://code.visualstudio.com/docs/setup/linux)

#### Sous Mac OS

Vous pouvez utiliser le terminal installé par défaut, situé dans `/Applications/Utilitaires/Terminal.app`.

Pour l'éditeur, [vous pouvez utiliser VSCode encore une fois](https://code.visualstudio.com/docs/setup/mac), mais si vous en préférez un autre (atom, vim, emacs, etc.), il n'y a pas de contre-indications. 

**Si vous rencontrez le moindre souci, n'hésitez pas à me contacter**

### Compte sur la plateforme gitlab de l'UGA

Vous aurez également besoin de vous créer un compte sur la plateforme gitlab de GRICAD qui est celle que nous allons utiliser pour la formation. 

Comme vous êtes externes à l'UGA, il faudra vous créer un compte sur cette page : 
https://register.gricad-gitlab.univ-grenoble-alpes.fr/index.html

Ensuite, vous pourrez vous connecter sur cette page, (**en cliquant sur l'onglet standard avant de mettre votre login/mdp**) : 
https://gricad-gitlab.univ-grenoble-alpes.fr/users/sign_in

**Encore une fois, si vous rencontrez le moindre souci, contactez-moi.**

### Rudiments d'utilisation d'un terminal

Dans cette formation, nous allons utiliser un terminal. Si vous n'êtes pas familier avec cet environnement, je vous suggère de vous entraîner un peu avant. 

Sous linux, vous pouvez utiliser l'application terminal. 

Sous windows (*que je ne connais que très peu*), vous pouvez utiliser VSCode. Une fois VScode ouvert, vous pouvez afficher un terminal en cliquant dans le menu terminal en haut de la fenêtre ou en utilisant le raccourci clavier CTRL+SHIFT+`.  


Il faudrait au moins connaître les commandes suivantes : 
* `cd /chemin/vers/un/répertoire` qui vous permet de changer de répertoire
* `ls` qui permet de lister les fichiers et dossiers du répertoire dans lequel vous êtes
* `touch file.txt` commande qui va créer le fichier file.txt vide.
* `mkdir nomDeDossier` commande qui permet de créer un dossier appelé nomDeDossier dans le répertoire courant
* `rm unfichier` et `rm -r undossier` qui suppriment respectivement un fichier et un dossier
* `pwd` vous affiche le chemin complet du dossier dans lequel vous vous trouvez. 

**Si vous n'êtes pas à l'aise avec le terminal, merci de m'informer. En fonction de ces retours, j'adapterai l'introduction du stage.** 




---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "31 mai 2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Stage Git & GitLab - Support
## URFIST de Lyon 

*pierre-antoine.bouttier@univ-grenoble-alpes.fr*

31 mai 2022

- [Introduction](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/introduction.html) - [PDF](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/introduction.pdf)
- [Git](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/git.html) - [PDF](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/git.pdf)
- [GitLab](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/gitlab.html) - [PDF](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/gitlab.pdf)
- [Git remote](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/git_remote.html) - [PDF](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/git_remote.pdf)
- [Miscellanées](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/misc.html) - [PDF](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/misc.pdf)

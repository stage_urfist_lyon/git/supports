---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "31 mai 2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Stage Git & GitLab - Introduction
## URFIST de Lyon 

*pierre-antoine.bouttier@univ-grenoble-alpes.fr*

31 mai 2022

---
## D'où je parle

* Ingénieur de recherche CNRS, expert en calcul scientifique...
* ...Au sein de l'UAR GRICAD...
* ...Responsable de l'équipe Calcul Scientifique et Données
* Formateur pour le CED UGA ; 
* **Formation niveau débutant : une ~~première~~ deuxième pour moi !**
* Très faible connaissance de windows

---
## Déroulement

* Cette introduction - *1h*
* **Git** Cours et Mise en pratique - *2h30*
* **GitLab** Cours et Mise en pratique - *2h30* 
* Mélange cours magistral et mise en pratique : 
  * **Ne surtout pas hésitez à m'interrompre**
  * **Expérimentez !**
  * L'interactivité est la réelle plus-value du stage
* **Objectif** : 
  * Être autonome pour la gestion d'un dépôt git
  * Savoir utiliser gitlab, faire le lien entre git et gitlab
  * Commencer à savoir travailler à plusieurs sur un projet Git/Gitlab

--- 
## Tour de salle

- Vos noms et prénoms, bien sûr
- Votre fonction
- Votre connaissance de l'informatique (bureautique, développement, connaissance du terminal, etc.)
- Dans quel cadre pensez-vous avoir besoin de Git et GitLab ?

---
# Vérification des prérequis

---
## Les outils

- Installation de git et d'un éditeur de texte ? 

---
## Les outils

- ~~Installation de git et d'un éditeur de texte ?~~
- Utilisation basique d'un terminal ? 

### Sous windows

On utilisera VSCode.

### Sous Linux/Mac OS

On utilisera l'application/le logiciel `terminal` ou `Terminal.app`. 

---
## Utilisation basique d'un terminal - Définitions

- Quelques définitions très simples : 
  - **Terminal** : interface textuelle pour la saisie de commandes.
  - **Commandes** : Logiciels avec une interface textuelle. Requiert souvent en entrée des **arguments** (*e.g.* chemins vers des fichiers et/ou dossiers) et des options.   
```shell
$ cmd --nomoption1 -nopt2 valopt2 <argument1> <argument2> # Exemple d'appel d'un cmd avec 2 options et 2 arguments
```
*`$` représente le prompt. Ne le recopiez pas. Le texte situé après `#` est un commentaire. Ne le recopiez pas non plus.*

***Lorsque qu'un bloc de code comme ci-dessus est présent, n'hésitez pas à taper les commandes dans votre terminal. Je laisserai toujours un peu de temps pour le faire.***

---
## Utilisation basique d'un terminal - commandes utiles

```shell
$ pwd # Renvoie le chemin vers le dossier dans lequel vous êtes dans le terminal (i.e. dossier courant)
$ ls # Liste les fichiers et dossiers du dossier dans lequel vous êtes
$ ls . # Qu'est-ce que ça fait ?
$ mkdir nouveau_dossier # Créer le dossier nommé nouveau dossier
$ cd nouveau_dossier
$ touch un_fichier_vide.txt # Créé le fichier nommé unfichier_vide.txt qui est... vide
$ touch un_autre.txt
$ ls
$ rm un_fichier_vide.txt # Supprime le fichier précédemment créé
$ cd .. # change de dossier courant en remontant d'un cran dans l'arborescence (les ..)
$ pwd
$ rm -r nouveau_dossier # Supprime le dossier nommé nouveau dossier (et ce qu'il contient). 
$ cd --help # Affiche l'aide de la commande cd. Ici, --help est une option de la commande cd 
```

Les commandes peuvent avoir des options, préfixées le plus souvent par `--` pour leur nom complet ou `-`pour leur nom abrégé. 

---
## Éditer un fichier depuis le terminal

### Sous windows avec VSCode

Dans le terminal VSCode : 
```shell
$ code chemin\vers\le\fichier.txt
```

### Sous Linux/Mac OS

Dans un terminal, plusieurs solutions en fonction des éditeurs installés : 
```shell
$ code chemin\vers\le\fichier.txt
$ gedit chemin\vers\le\fichier.txt
$ nano chemin\vers\le\fichier.txt # Ou vim, emacs pour les plus téméraires :)
$...
```

---
## Afficher rapidement le contenu d'un fichier texte
 
```shell
$ cat fichier.txt
$ less fichier.txt # alternative valable sous linux et mac os
```

---
## Home sweet home

Le `/home` est le répertoire le plus haut de l'utilisateur courant. Au-dessus, ce sont des répertoires systèmes :  
 
```shell

$ cd C:\Users\pa # Sous windows
$ cd /home/pa # Sous Linux/Mac OS
$ cd # Dans les deux cas
$ cd ~ # Dans les deux cas
$ ls -a # Petit bonus pour les fichiers cachés
```

---
## Mise en pratique...

...utile pour la suite ! Dans un terminal : 

* Placez vous dans votre `/home`
* Créez un dossier nommé `example_project` puis déplacer vous dedans
* Créez un fichier texte `first_file.md` dans ce dossier et sauvegardez-le.
* Afficher le contenu de ce fichier texte. 
* Modifiez-le
* Afficher de nouveau le fichier texte. 
* Sortez du dossier. 

---
## Les outils

- ~~Installation de git et d'un éditeur de texte ?~~
- ~~Utilisation basique d'un terminal ?~~
- Création d'un compte gitlab ? 

---
## Les outils

- ~~Installation de git et d'un éditeur de texte ?~~
- ~~Utilisation basique d'un terminal ?~~
- ~~Création d'un compte gitlab ?~~ 

[**On y va ?**](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/git/supports/git.html) 



